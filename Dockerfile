FROM openjdk:12
EXPOSE 8080
ADD ./target/tipo_cambio-0.0.1-SNAPSHOT.jar tipo_cambio.jar
ENTRYPOINT ["java","-jar","tipo_cambio.jar"]