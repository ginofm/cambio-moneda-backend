package com.bbva.cambio.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CambioMonedaInput {


	@NotEmpty(message = "no puede estar vacio")
	private Double montoBase;
	
	@NotEmpty(message = "no puede estar vacio")
	private String monedaDestino;
	
	@NotEmpty(message = "no puede estar vacio")
	private String monedaOrigen;
	

	
	
}
