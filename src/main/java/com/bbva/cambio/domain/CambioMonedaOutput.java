package com.bbva.cambio.domain;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CambioMonedaOutput {
	
	private Double monto;
	
	private Double montoCambio;
	
	private String monedaOrigen;
	
	private String monedaDestino;
	
	private Double tipoCambio;

}
