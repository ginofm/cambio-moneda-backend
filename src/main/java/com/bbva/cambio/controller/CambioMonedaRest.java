package com.bbva.cambio.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.cambio.domain.CambioMonedaInput;
import com.bbva.cambio.domain.CambioMonedaOutput;
import com.bbva.cambio.domain.TipoCambio;
import com.bbva.cambio.service.ITipoCambioService;

@RestController
@RequestMapping("/api")
public class CambioMonedaRest {

	@Autowired
	private ITipoCambioService tipoCambioService;
	
	@PostMapping("/cambioMoneda")
	public ResponseEntity<?> create(@RequestBody CambioMonedaInput cambioMoneda){
		
		
		TipoCambio tipoCambioDestino = null;
		CambioMonedaOutput cambioMonedaOut = new CambioMonedaOutput();
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			tipoCambioDestino = tipoCambioService.findByTipoCambio(cambioMoneda.getMonedaDestino());
			cambioMonedaOut.setMonto(cambioMoneda.getMontoBase());
			cambioMonedaOut.setMontoCambio(cambioMoneda.getMontoBase() * tipoCambioDestino.getPrecio());
			cambioMonedaOut.setMonedaOrigen(cambioMoneda.getMonedaOrigen());
			cambioMonedaOut.setMonedaDestino(cambioMoneda.getMonedaDestino());
			cambioMonedaOut.setTipoCambio(tipoCambioDestino.getPrecio());
	
		} catch(Exception e) {
			
			response.put("mensaje", "Error al realizar la operacion");
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","El proceso se realizo con éxito");
		response.put("cambioMoneda", cambioMonedaOut);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
}
