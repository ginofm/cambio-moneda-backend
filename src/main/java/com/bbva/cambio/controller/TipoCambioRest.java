package com.bbva.cambio.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.cambio.domain.TipoCambio;
import com.bbva.cambio.service.ITipoCambioService;

@RestController
@RequestMapping("/api")
public class TipoCambioRest {
	
	@Autowired
	private ITipoCambioService tipoCambioService;
	
	@PutMapping("/tipoCambio/{id}")
	public ResponseEntity<?> update(@RequestBody TipoCambio tipoCambio, @PathVariable Integer id){
		
		TipoCambio tipoCambioAct = tipoCambioService.findById(id);
		TipoCambio tipoCambioUpd = null;
		
		Map<String, Object> response = new HashMap<>();
		
		if(tipoCambioAct == null) {
			response.put("mensaje", "Error: no se puede editar, el Tipo de Cambio - ID: ".concat(id.toString().concat(" no existe en la base de datos")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		
		try {
			tipoCambioAct.setNombre(tipoCambio.getNombre());
			tipoCambioAct.setPrecio(tipoCambio.getPrecio());
			tipoCambioUpd = tipoCambioService.save(tipoCambioAct);
			
		} catch(DataAccessException e) {
			
			response.put("mensaje", "Error al actualizar en la base de datos");
			response.put("error", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","El Tipo de cambio ha sido actualizado con éxito");
		response.put("tipoCambio", tipoCambioUpd);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}


}
