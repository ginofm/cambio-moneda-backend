package com.bbva.cambio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cambio.domain.TipoCambio;
import com.bbva.cambio.repositorio.ITipoCambioRepo;
import com.bbva.cambio.service.ITipoCambioService;

@Service
public class TipoCambioServiceImpl implements ITipoCambioService{

	@Autowired
	private ITipoCambioRepo tipoCambioRepo;
	
	@Override
	public TipoCambio findByTipoCambio(String nombre) {
		// TODO Auto-generated method stub
		return tipoCambioRepo.findByTipoCambio(nombre);
	}

	@Override
	public List<TipoCambio> findAll() {
		// TODO Auto-generated method stub
		return tipoCambioRepo.findAll();
	}

	@Override
	public TipoCambio save(TipoCambio tipoCambio) {
		// TODO Auto-generated method stub
		return tipoCambioRepo.save(tipoCambio);
	}
	
	@Override
	public void saveAll(List<TipoCambio> tipoCambio) {
		// TODO Auto-generated method stub
		 tipoCambioRepo.saveAll(tipoCambio);
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		tipoCambioRepo.deleteById(id);
	}

	@Override
	public TipoCambio findById(Integer id) {
		// TODO Auto-generated method stub
		return tipoCambioRepo.findById(id).orElse(null);
	}


	
}
