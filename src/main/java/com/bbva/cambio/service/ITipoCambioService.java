package com.bbva.cambio.service;

import java.util.List;

import com.bbva.cambio.domain.TipoCambio;

public interface ITipoCambioService {
	
	public TipoCambio findByTipoCambio(String nombre);

	public List<TipoCambio> findAll();
	
	public TipoCambio findById(Integer id);
	
	public TipoCambio save(TipoCambio tipoCambio);
	
	public void saveAll(List<TipoCambio> tipoCambio);
	
	public void deleteById(Integer id);
}
