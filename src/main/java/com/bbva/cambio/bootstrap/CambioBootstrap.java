package com.bbva.cambio.bootstrap;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.bbva.cambio.domain.TipoCambio;
import com.bbva.cambio.service.ITipoCambioService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CambioBootstrap implements ApplicationListener<ContextRefreshedEvent>{
	
	private ITipoCambioService tipoCambioService;
	
	public CambioBootstrap(ITipoCambioService tipoCambioService) {
		this.tipoCambioService = tipoCambioService;
	}


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// TODO Auto-generated method stub
		tipoCambioService.saveAll(getTipoCambio());
		log.debug(getTipoCambio().toString());
	}
	
	private List<TipoCambio> getTipoCambio(){
		
		List<TipoCambio> tipoCambios = new ArrayList<>();
		
		TipoCambio dolar = TipoCambio.builder()
				.nombre("dolar")
				.precio(3.63)
				.build();
		
		TipoCambio libra = TipoCambio.builder()
				.nombre("libra")
				.precio(5.20)
				.build();
		
		TipoCambio euro = TipoCambio.builder()
				.nombre("libra")
				.precio(4.47)
				.build();
		
		tipoCambios.add(dolar);
		tipoCambios.add(libra);
		tipoCambios.add(euro);
		
		return tipoCambios;
	}

	
	
}
