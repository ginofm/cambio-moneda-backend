package com.bbva.cambio.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bbva.cambio.domain.TipoCambio;

public interface ITipoCambioRepo extends JpaRepository<TipoCambio, Integer>{
	
	@Query("select tc from TipoCambio tc where tc.nombre = ?1")
	public TipoCambio findByTipoCambio(String nombre);
	
	

}
